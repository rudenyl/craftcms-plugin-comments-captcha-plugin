<?php
namespace Craft;

class Mathcha extends CaptchaBaseModule
{
    const HASH_KEY = 'mathcha:k81Ffqzqzx6Pd5qYs8YTD9l3q7FPIaEO';

    public function render()
    {
        $operations = [
            '&#43;', 
            '&minus;', 
            '&times;'
        ];

        $action = rand(0, count($operations) - 1);
        $expression = [];
        $expression[0] = rand(1, 9);
        $expression[1] = rand(1, 9);

        switch ($operations[$action]) 
        {
            case '&#43;':
                $expression[2] = $expression[0] + $expression[1];
                break;

            case '&minus;':
                if ($expression[0] < $expression[1]) 
                {
                    $number = $expression[0];

                    $expression[0] = $expression[1];
                    $expression[1] = $number;
                }

                $expression[2] = $expression[0] - $expression[1];
                break;

            case '&times;':
                $expression[2] = $expression[0] * $expression[1];
                break;
        }

        // randomize input position
        $rand_input = rand(0, 2);

        $allowed_formats = ['number', 'word'];
        $operand_formats = [];

        $max_rand_value = count($allowed_formats) - 1;
        for ($i = 0; $i < 3; $i++) 
        {
            $operand_formats[] = $rand_input == $i ? 'input' : $allowed_formats[mt_rand(0, $max_rand_value) ];
        }

        $operand = [];
        foreach($operand_formats as $key => $format) 
        {
            switch ($format) 
            {
                case 'input':
                    $operand[] = '<input id="cptchpr_input" class="cptchpr_input" type="text" autocomplete="off" name="cptchpr_number" value="" maxlength="2" size="2" aria-required="true" required="required" style="margin-bottom:0;display:inline;font-size: 12px;width: 40px;" />';
                    break;

                case 'word':
                    $operand[] = $this->generate_value($expression[$key]);
                    break;

                case 'number':
                default:
                    $operand[] = $expression[$key];
                    break;
            }
        }

        // include resources
        craft()->templates->includeJsFile('/assets/mathcha/mathcha.js');
        craft()->templates->includeCssFile('/assets/mathcha/styles.css');

        $timestamp = microtime(true);
        $key = $this->_encode($expression[$rand_input], $this->options['key'], $timestamp);

        $html = <<<HTML
<div class="cptchpr_wrap" id="cptchpr_{$timestamp}">
    <span>{$operand[0]}</span>
    <span>&nbsp;{$operations[$action]}&nbsp;</span>
    <span>{$operand[1]}</span>
    <span>&nbsp;=&nbsp;</span>
    <span style="margin-right: 15px;">{$operand[2]}</span>

    <input type="hidden" name="cptchpr_result" value="{$key}" />
    <input type="hidden" name="cptchpr_time" value="{$timestamp}" />

    <a class="captcha-button reload" href="#" data-target="cptchpr_{$timestamp}" data-reload="/comments/plugins/captcha/modules/mathcha">Reload</a>
</div>
HTML;

        if (isset($this->options['template']) && !empty($this->options['template']))
        {
            $html = sprintf($this->options['template'],
                $operand[0],
                $operations[$action],
                $operand[1],
                $operand[2]
            );
        }

        return $html;
    }

    public function actionShow()
    {
        $html = $this->render();

        if (craft()->request->isAjaxRequest()) 
        {
            $this->returnJson(compact('html'));
        }
        else 
        {
            $this->returnHTML($html);
        }
    }

    public function registerListeners()
    {
        // Event listeners
        craft()->on('comments.onBeforeSaveComment', function($event) 
        {
            if (craft()->request->isCpRequest())
            {
                return true;
            }

            if (!$this->check())
            {
                $event->performAction = false;

                $event->params['comment']->addErrors([
                    'comment' => 'Captcha don\'t match.'
                ]);
            }
        });
    }

    public function registerSiteRoutes()
    {
        return [
            'comments/plugins/captcha/modules/mathcha' => [
                'action' => 'comments/captcha/show',
                'params' => [
                    'action' => 'show'
                ]
            ],
        ];
    }

    public function getSettingsHtml($settings, $action = null)
    {
        //
        // return custom action, if exists
        //
        switch ($action)
        {
            case 'generate_hash':
                return StringHelper::UUID();
        }

        return (new CommentsTemplateService)->render('captcha/modules/mathcha/settings', compact('settings'));
    }

    // getter
    public function __get($property)
    {
        if ($property == 'options')
        {
            $settings = $this->getSettings();
            if (!isset($settings['key']) or empty($settings['key']))
            {
                $settings['key'] = $this->HASH_KEY;
            }

            return $settings;
        }
        else if (property_exists($this, $property))
        {
            return $this->$property;
        }
    }


    // Protected Methods
    // =========================================================================

    protected function check()
    {
        $number = craft()->request->getPost('cptchpr_number');
        $context = craft()->request->getPost('cptchpr_result');
        $timestamp = craft()->request->getPost('cptchpr_time');

        $code = $this->_decode($context, $this->options['key'], $timestamp);

        return $code == $number;
    }

    protected function convert_letters($context)
    {
        if (isset($this->options['difficulty_word']) && (int)$this->options['difficulty_word']) 
        {
            $chars = [
                'a' => '&#97;',                
                'b' => '&#98;',                
                'c' => '&#99;',                
                'd' => '&#100;',
                'e' => '&#101;',
                'f' => '&#102;',
                'g' => '&#103;',
                'h' => '&#104;',
                'i' => '&#105;',
                'j' => '&#106;',
                'k' => '&#107;',
                'l' => '&#108;',
                'm' => '&#109;',
                'n' => '&#110;',
                'o' => '&#111;',
                'p' => '&#112;',
                'q' => '&#113;',
                'r' => '&#114;',
                's' => '&#115;',
                't' => '&#116;',
                'u' => '&#117;',
                'v' => '&#118;',
                'w' => '&#119;',
                'x' => '&#120;',
                'y' => '&#121;',
                'z' => '&#122;',
            ];

            $length = strlen($context);
            $length--;

            $str = str_split($context);
            $convert_letters = rand(1, $length);

            while ($convert_letters != 0) 
            {
                $position = rand(0, $length);
                $str[$position] = isset($chars[$str[$position]]) ? $chars[$str[$position]] : $str[$position];
                $convert_letters--;
            }

            $context = '';
            foreach($str as $key => $value) 
            {
                $context .= $value;
            }
        }
        
        return $context;
    }

    protected function generate_value($value, $use_only_words = true)
    {
        $random = $use_only_words ? true : mt_rand(0, 1);
        if ((int)$random)
        {
            $numbers = [
                '0_to_9' => [
                    'zero',
                    'one',
                    'two',
                    'three',
                    'four',
                    'five',
                    'six',
                    'seven',
                    'eight',
                    'nine',
                ],
                '11_to_19' => [
                    1 => 'eleven',
                    'twelve',
                    'thirteen',
                    'fourteen',
                    'fifteen',
                    'sixteen',
                    'seventeen',
                    'eighteen',
                    'nineteen',
                ],
                '10_to_90' => [
                    1 => 'ten',
                    'twenty',
                    'thirty',
                    'forty',
                    'fifty',
                    'sixty',
                    'seventy',
                    'eighty',
                    'ninety',
                ]
            ];


            if ($value < 10) 
            {
                return $this->convert_letters($numbers['0_to_9'][$value]);
            }
            elseif ($value < 20 && $value > 10)
            {
                return $this->convert_letters($numbers['11_to_19'][$value % 10]);
            }
            else 
            {
                return $this->convert_letters($numbers['10_to_90'][$value / 10]) . '&nbsp;' . 
                    (0 != $value % 10 ? $this->convert_letters($numbers['0_to_9'][$value % 10]) : '');
            }
        }

        return $value;
    }


    // Private Methods
    // =========================================================================

    //
    // Encoding/decoding methods
    //
    private function _encode($context, $password, $timestamp)
    {
        $salt = md5($timestamp, true);

        $context = substr(pack("H*", sha1($context)) , 0, 1) . $context;

        $length = strlen($context);
        $sequence = $password;
        $gamma = '';

        while (strlen($gamma) < $length)
        {
            $sequence = pack("H*", sha1($sequence . $gamma . $salt));
            $gamma .= substr($sequence, 0, 8);
        }

        return base64_encode($context ^ $gamma);
    }

    private function _decode($context, $key, $timestamp)
    {
        $salt = md5($timestamp, true);

        $length = strlen($context);
        $sequence = $key;
        $gamma = '';

        while (strlen($gamma) < $length)
        {
            $sequence = pack("H*", sha1($sequence . $gamma . $salt));
            $gamma .= substr($sequence, 0, 8);
        }

        $context = base64_decode($context);
        $context = $context ^ $gamma;
        $decoded_context = substr($context, 1);

        $result_error = ord(substr($context, 0, 1) ^ substr(pack("H*", sha1($decoded_context)) , 0, 1));
        return $result_error ? false : $decoded_context;
    }
}
