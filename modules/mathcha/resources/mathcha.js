if (typeof jQuery == 'function') {
    var xhr;
    $(function() {
        $(document).on('click', '.cptchpr_wrap .captcha-button[data-reload]', function(e) {
            e.preventDefault();

            var data = $(this).data() || {};
            var target = data.target;
            if (!data.reload) return;

            xhr && xhr.abort();
            xhr = $.get(data.reload, function(res) {
                // error trapping intentionally left open
                $('[id="%s"]'.replace(/%s/, target)).replaceWith( $(res.html) );
            });
        });
    });
}