<?php
namespace Craft;

class Comments_CaptchaController extends CommentsPluginController
{
    protected $allowAnonymous = true;

    public function actionSettings()
    {
        // get plugin settings
        $settings = $this->getSettings();

        // get available modules
        $modules = [];
        $modules[] = new OptionData('Select module', null, false);
        
        if ($tmp = Comments_CaptchaService::getModules())
        {
            foreach ($tmp as $module)
            {
                $name = mb_strtolower($module['class']);

                $modules[$name] = $module['name'];
            }
        }

        $this->renderTemplate('captcha/settings', compact('settings', 'modules'));
    }

    public function actionModuleSettings()
    {
        $module = craft()->request->getQuery('module');

        if ($module = Comments_CaptchaService::getModule($module))
        {
            // has custom action? attach
            $action = craft()->request->getQuery('do');

            $html = $module->getSettingsHtml($this->getModuleSettings(), $action);

            if (craft()->request->isAjaxRequest()) 
            {
                $this->returnJson(compact('html'));
            }

            ob_start();
            echo $html;

            craft()->end();
        }
    }

    public function actions()
    {
        if ($module = $this->getActiveModule())
        {
            return $module->registerActions();
        }
    }


    // Protected Methods
    // =========================================================================

    protected function getActiveModule()
    {
        // get plugin settings
        $settings = $this->getSettings();

        // get active Captcha module
        $module = Comments_CaptchaService::getModule($settings->captcha_module);

        return $module;
    }

    protected function getSettings()
    {
        // get plugin settings
        return craft()->comments_plugins->getPluginSettings('Captcha');
    }

    protected function getModuleSettings($name = null)
    {
        // get plugin settings
        $settings = $this->getSettings();

        if (empty($name))
        {
            $name = $settings->captcha_module;
        }

        if ($settings && isset($settings->module_data) && isset($settings->module_data[$name]))
        {
            return $settings->module_data[$name];
        }

        return [];
    }
}