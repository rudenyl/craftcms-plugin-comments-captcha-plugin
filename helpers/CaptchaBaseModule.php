<?php
namespace Craft;

abstract class CaptchaBaseModule
{
    protected $_enabled;

    abstract function render();

    public function registerCpRoutes()
    {
        return [];
    }

    public function registerSiteRoutes()
    {
        return [];
    }

    public function registerListeners(){}

    /**
     * To register an action, make sure to add a 'action' key to the params
     * that maps to a module method.
     *
     * [
     *   'action' => 'Controller/action',
     *   'params' => ['action' => 'your_controller_method']
     * ]
     */
    public function registerActions()
    {
        $actions = [];

        $class = get_class($this);
        foreach (get_class_methods($class) as $name)
        {
            if (preg_match('/action(\w+)/', $name, $match))
            {
                $action = trim($name, 'action');
                $action = mb_strtolower($action);

                $actions[$action] = $class;
            }
        }

        return $actions;
    }

    public function run($method, $args = [])
    {
        $method_name = 'action' . ucwords($method);
        if (method_exists($this, $method_name))
        {
            return call_user_func_array([$this, $method_name], [$args]);
        }
    }

    public function runWithParams($params)
    {
        $method = new \ReflectionMethod($this, 'run');

        $args = null;
        if (isset($params['variables']))
        {
            if (isset($params['variables']['matches'][1]))
            {
                $args = $params['variables']['matches'][1];
            }
        }

        $this->run(isset($params['action']) ? $params['action'] : null, $args);

        return true;
    }

    public function getSettings()
    {
        $class = get_class($this);
        $class = mb_strtolower(trim($class, 'Craft\\'));

        // get plugin settings
        $settings = craft()->comments_plugins->getPluginSettings('Captcha');

        if ($settings && isset($settings->module_data) && isset($settings->module_data[$class]))
        {
            return $settings->module_data[$class];
        }

        return [];
    }

    public function getSettingsHtml($settings, $action = null)
    {
        return false;
    }

    public function returnHTML($context)
    {
        ob_start();
        echo $context;

        craft()->end();
    }

    public function returnJson($var = array(), $options = array())
    {
        JsonHelper::setJsonContentTypeHeader();

        $options = array_merge([
            'expires' => false,
        ], $options);

        // Set the Expires header
        if ($options['expires'] === false)
        {
            HeaderHelper::setNoCache();
        }
        else if ($options['expires'])
        {
            HeaderHelper::setExpires($options['expires']);
        }

        ob_start();
        echo JsonHelper::encode($var);

        craft()->end();
    }
}