<?php
namespace Craft;

class CaptchaCommentsPlugin extends CommentsBasePlugin
{
    /**
     * @object Active Captcha module
     */
    protected $_captcha_module;

    public function getName()
    {
        return 'Captcha module for Comments plugin';
    }

    public function getVersion()
    {
        return '1.0.0';
    }

    public function getDeveloper()
    {
        return 'AspenCoreDev Team';
    }

    public function getDeveloperUrl()
    {
        return 'http://www.innovuze.com';
    }

    public function getDescription()
    {
        return Craft::t('Simple captcha module for Comments plugin.');
    }

    public function init()
    {
        // load dependencies
        Craft::import('plugins.comments.plugins.captcha.helpers/*');

        // get plugin settings
        $settings = craft()->comments_plugins->getPluginSettings('Captcha');

        // get active Captcha module
        $this->_captcha_module = Comments_CaptchaService::getModule($settings->captcha_module);

        // Event listeners
        if ($this->_captcha_module)
        {
            $this->_captcha_module->registerListeners();
        }

        // load hooks
        craft()->request->isCpRequest() and craft()->templates->hook('captcha.module.settings', function(&$context) use($settings)
        {
            if ($this->_captcha_module)
            {
                $variables = [];
                if ($settings && isset($settings->module_data) && isset($settings->module_data[$settings->captcha_module]))
                {
                    $variables = $settings->module_data[$settings->captcha_module];
                }

                return $this->_captcha_module->getSettingsHtml($variables);
            }
        });
    }

    public function afterFormDisplay()
    {
        if ($this->_captcha_module)
        {
            return $this->_captcha_module->render();
        }
    }

    public function registerCpRoutes()
    {
        $routes = [];

        if ($this->_captcha_module)
        {
            $routes = array_merge($routes, $this->_captcha_module->registerCpRoutes());
        }

        return array_merge($routes, [
            'comments/plugins/captcha/settings' => [
                'action' => 'comments/captcha/settings'
            ],
            'comments/plugins/captcha/module/settings' => [
                'action' => 'comments/captcha/modulesettings'
            ],
        ]);
    }

    public function registerSiteRoutes()
    {
        $routes = [];

        if ($this->_captcha_module)
        {
            $routes = array_merge($routes, $this->_captcha_module->registerSiteRoutes());
        }

        return $routes;
    }

    public function getSettingsUrl()
    {
        return 'plugins/captcha/settings';
    }

    protected function defineSettings()
    {
        return [
            'captcha_module' => AttributeType::String,
            'module_data' => AttributeType::String,
        ];
    }
}
