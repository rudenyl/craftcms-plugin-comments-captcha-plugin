<?php
namespace Craft;

class Comments_CaptchaService extends BaseApplicationComponent
{
    public static function getModule($moduleClass)
    {
        $modules = static::getModules();

        if (isset($modules[$moduleClass]))
        {
            $json = $modules[$moduleClass];

            // import
            $_class = $json['class'];
            $file = sprintf('%s.%s', $moduleClass, $_class);

            Craft::import('plugins.comments.plugins.captcha.modules.' . $file, true);

            // init Craft class
            $class = '\\Craft\\' . $_class;

            return new $class;
        }

        return false;
    }

    public static function getModules()
    {
        $path = craft()->path->getPluginsPath() . '/comments/plugins/captcha/modules';

        $modules = [];

        if (IOHelper::folderExists($path))
        {
            $filter = 'module\.json$';
            $files = IOHelper::getFolderContents($path, true, $filter);

            if ($files)
            {
                foreach ($files as $file)
                {
                    $contents = IOHelper::getFileContents($file);

                    $json = JsonHelper::decode($contents);

                    // get class
                    if (!isset($json['class']))
                    {
                        continue;
                    }

                    // add path
                    $json['path'] = dirname($file);

                    $class = mb_strtolower($json['class']);
                    $modules[$class] = $json;
                }
            }
        }

        return $modules;
    }
}
